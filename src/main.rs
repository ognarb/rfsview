extern crate walkdir;
extern crate random_color;

use std::env;
use walkdir::WalkDir;
use std::fs::read_dir;
use std::path::Path;
use std::fs::Metadata;
use random_color::RandomColor;

fn main() {
    let args: Vec<_> = env::args().collect();
    let dir: &str = "";
    if (args.len() < 2) {
        read_directory(".");
    } else {
        read_directory(&args[1]);
    }
}

fn get_size(path: &Path, metadata: Metadata) -> std::io::Result<u64> {
    let mut size = metadata.len();
    if path.is_dir() {
        for entry in read_dir(path)? {
            if let Ok(entry) = entry {
                if let Ok(metadata) = entry.metadata() {
                    size += get_size(&entry.path(), metadata).unwrap();
                }
            }
        }
    }
    if size == 0 {
        size = 1;
    }
    Ok(size)
}

fn get_child_number(path: &Path) -> std::io::Result<u64> {
    let mut size = 0;
    if path.is_dir() {
        for entry in read_dir(path)? {
            let ref e = &entry?;
            if !e.file_type()?.is_symlink() && !(e.file_type()?.is_dir() && get_child_number(&e.path())? == 0) {
                size += 1;
            }
        }
    }
    Ok(size)
}

fn read_directory(dir_name: &str) -> std::io::Result<()> {
    let root = Path::new(dir_name);

    if root.is_dir() {
        if get_child_number(root)? == 0 {
            // to nothing
        } else {
            print!("{}\n", get_child_number(root)?);
        }
    }


    for entry in WalkDir::new(dir_name) {
        let ref e = &entry?;

        // identation
        for number in (0..(e.depth())).rev() {
            print!("  ");
        }
        
        // Entry 
        let color = RandomColor::new().to_rgb_array();
        print!("{} {} {} {}\n", get_size(e.path(), e.metadata()?)?, color[0], color[1], color[2]);

        if e.file_type().is_dir() {
            if get_child_number(e.path())? == 0 {
                // to nothing
            } else {
                print!("- {}\n", get_child_number(e.path())?);
            }
        }
    }
    Ok(())
}
